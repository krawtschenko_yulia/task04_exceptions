package me.krawtschenko.yulia.view;

import java.util.ArrayList;

public class BookLookUpView extends View {
    public BookLookUpView() {
        super();

        MenuItem findOne = new MenuItem("1", "Find");
        super.menu.put("1", findOne);

        MenuItem findAll = new MenuItem("2", "Find All");
        super.menu.put("2", findAll);
    }

    public String requestSearchQuery() {
        String query = "";

        while (query.isBlank()) {
            System.out.print("Search: ");
            query = input.nextLine();
        }

        return query;
    }

    public void displayFound(String foundLine) {
        System.out.println("Found (first occurrence): ");
        System.out.println(foundLine);
    }

    public void displayFound(ArrayList<String> foundLines) {
        System.out.println("Found: ");
        foundLines.forEach(System.out::println);
    }
}
