package me.krawtschenko.yulia.view;

public class MenuItem {
    private String key;
    private String description;
    private MenuAction action;

    MenuItem(String key, String description) {
        this.key = key;
        this.description = description;
    }

    public void linkAction(MenuAction action) {
        this.action = action;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public MenuAction getAction() {
        return action;
    }
}
