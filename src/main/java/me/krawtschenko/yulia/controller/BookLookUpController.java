package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BookLookUpModel;
import me.krawtschenko.yulia.model.NotFoundException;
import me.krawtschenko.yulia.view.BookLookUpView;
import me.krawtschenko.yulia.view.MenuAction;

import java.util.ArrayList;

public class BookLookUpController {
    private BookLookUpModel model;
    private BookLookUpView view;

    private MenuAction findOneBook;
    private MenuAction findManyBooks;
    private MenuAction quit;

    private String pathToFile = "library.txt";

    public BookLookUpController() {
        model = new BookLookUpModel(pathToFile);
        view = new BookLookUpView();

        findOneBook = new MenuAction() {
            @Override
            public void execute() {
                String query = view.requestSearchQuery();
                String result = "";

                try {
                    result = model.findOne(query);
                    if (result != null) {
                        view.displayFound(result);
                    }
                } catch (NotFoundException e) {
                    view.printMessage(e.getMessage());
                }
            }
        };
        view.addMenuItemAction("1", findOneBook);

        findManyBooks = new MenuAction() {
            @Override
            public void execute() {
                String query = view.requestSearchQuery();
                ArrayList<String> results = new ArrayList<>();

                try {
                    results = model.findMany(query);
                    if (results != null) {
                        view.displayFound(results);
                    }
                } catch (NotFoundException e) {
                    view.printMessage(e.getMessage());
                }
            }
        };
        view.addMenuItemAction("2", findManyBooks);

        quit = new MenuAction() {
            @Override
            public void execute() {
                System.exit(0);
            }
        };
        view.addMenuItemAction("q", quit);
    }

    public void startMenu() {
        view.displayMenu();
        view.awaitValidMenuOption();
    }
}
