package me.krawtschenko.yulia.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class LibraryLoader implements AutoCloseable {
    private BufferedReader reader;

    LibraryLoader(Path path) throws IOException {
        reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
    }

    public String readLine() throws IOException {
        return reader.readLine();
    }

    @Override
    public void close() throws Exception {
        if (reader != null) {
            reader.close();
        }

        // throw new UnexpectedOutcomeException("Demo exception.");
    }
}
