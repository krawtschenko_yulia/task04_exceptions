package me.krawtschenko.yulia.model;

public class UnexpectedOutcomeException extends Exception {
    UnexpectedOutcomeException() {
        super();
    }

    public UnexpectedOutcomeException(String message) {
        super(message);
    }

    UnexpectedOutcomeException(Exception ex) {
        super(ex);
    }
}