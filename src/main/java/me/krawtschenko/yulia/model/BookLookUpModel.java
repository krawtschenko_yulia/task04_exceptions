package me.krawtschenko.yulia.model;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BookLookUpModel {
    private Path path;

    public BookLookUpModel(String pathToFile) {
        path = Paths.get(pathToFile);
        System.out.println(path.toAbsolutePath());
    }

    public String findOne(String query) {
        String line = null;

        try (LibraryLoader library = new LibraryLoader(path)) {
            boolean found = false;

            while (!found && (line = library.readLine()) != null) {
                if (line.toLowerCase().contains(query.trim().toLowerCase())) {
                    found = true;

                    break;
                }
            }
            if (!found) {
                throw new NotFoundException("Not found: " + query);
            }
        } catch (Exception e) {
            System.err.println("Primary: " + e.getMessage());

            for (Throwable t : e.getSuppressed()) {
                System.err.println("Suppressed: " + t.getMessage());
            }
        }

        return line;
    }

    public ArrayList<String> findMany(String query) {
        ArrayList<String> results = new ArrayList<>();

        try (LibraryLoader library = new LibraryLoader(path)) {
            boolean found = false;
            String line = null;

            while ((line = library.readLine()) != null) {
                if (line.toLowerCase().contains(query.trim().toLowerCase())) {
                    found = true;
                    results.add(line);
                }
            }
            if (!found) {
                throw new NotFoundException("Nothing found: " + query);
            } else {
                return results;
            }
        } catch (Exception e) {
            System.err.println("Primary: " + e.getMessage());

            for (Throwable t : e.getSuppressed()) {
                System.err.println("Suppressed: " + t.getMessage());
            }
        }

        return results;
    }
}
