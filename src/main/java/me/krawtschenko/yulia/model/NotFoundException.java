package me.krawtschenko.yulia.model;

public class NotFoundException extends RuntimeException {
    NotFoundException() {
        super();
    }

    NotFoundException(String message) {
        super(message);
    }

    NotFoundException(RuntimeException ex) {
        super(ex);
    }
}
