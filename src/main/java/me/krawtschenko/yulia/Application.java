package me.krawtschenko.yulia;

import me.krawtschenko.yulia.controller.BookLookUpController;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        new BookLookUpController().startMenu();
    }
}
